# PrimeAres Change Log

## [0.0.5] - 2017-11-10
### Added
- Added paGetClient native to get the client holding a weapon
- g_holding array & CheckPickUp function for use with SDKUseHook
- paGetClient_test to primeares_tests.sp

### Changed
- SDKWeaponSwitchToHook to SDKWeaponSwitchPostHook
- Moved SDKWeaponDropHook in the source next to the other SDKHook functions
- Natives to belong to their own section in the source
- Cleaned up some Get/Set entProp calls

### Fixed
- Grenade launcher sometimes getting its clip stuck at 0 and going empty early
- Applying weapon features from the dat file to weapon spawns on the field
- Inability to pick up some weapons at spawn if you were holding an M16
- M60 from going dead during a reload when swapping to another primary

## [0.0.4] - 2017-09-10
### Changed
- Moved logic from player_use hook straight into SDKUseHook

### Fixed
- Ability to grab ammo for M60 and Grenade Launcher while reloading

## [0.0.3] - 2017-09-10
### Changed
- The method of displaying zero for a primary weapon
- Pause time for m60 switching (from 1.0 to 0.25)

### Fixed
- Inability to pick up a weapon of the same type if held weapon is empty
- Bots not resupplying at ammo caches

## [0.0.2] - 2017-01-10
### Fixed
- Reflection of current upgrade on the HUD

## [0.0.1] - 2017-30-09
### Added
- Initial Release

## This CHANGELOG follows http://keepachangelog.com/en/0.3.0/
### CHANGELOG legend

- Added: for new features.
- Changed: for changes in existing functionality.
- Deprecated: for once-stable features removed in upcoming releases.
- Removed: for deprecated features removed in this release.
- Fixed: for any bug fixes.
- Security: to invite users to upgrade in case of vulnerabilities.
- [YANKED]: a tag too signify a release to be avoided.

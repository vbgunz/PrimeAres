# PrimeAres SourceMod Plugin

If you enjoy SourceMod and its community consider helping them them meet their monthly goal [here](http://sourcemod.net/donate.php). Your help, no matter the amount goes a long way in keeping a great project like SourceMod what it is... AWESOME.

Thanks :)

## License
PrimeAres a SourceMod L4D2 Plugin
Copyright (C) 2017  Victor "NgBUCKWANGS" Gonzalez

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## About
PrimeAres keeps weapons at 0 useable. It makes the M60 and Grenade Launchers capable of being reloaded. It offers the ability to customize a guns features when picked up from a spawn. It exposes natives for use in other plugins.

### Installing PrimeAres
If you have the full package from e.g., [my GitLab package](https://gitlab.com/vbgunz/PrimeAres/repository/master/archive.zip), you can unzip it and drop the addons folder contents right on top of ```.../left4dead2/addons``` and skip steps 1 through 5. If you restart your server after dropping the files you can skip steps 6 through 7.

> Note: Some of these files may not exists at all.

1. Copy primeares.smx .../left4dead2/addons/sourcemod/plugins/primeares.smx
2. Copy primeares_tests.smx .../left4dead2/addons/sourcemod/plugins/primeares_tests.smx
3. Copy primeares.inc .../left4dead2/addons/sourcemod/scripting/include/primeares.inc
4. Copy primeares.dat .../left4dead2/addons/sourcemod/data/primeares.dat
5. Copy primeares.cfg .../left4dead2/cfg/sourcemod/primeares.cfg
6. ```sm_rcon sm plugins load primeares```
7. ```sm_rcon sm plugins load primeares_tests```

### Uninstalling PrimeAres

> Note: Some of these files may not exists at all.

1. Remove .../left4dead2/addons/sourcemod/plugins/primeares.smx
2. Remove .../left4dead2/addons/sourcemod/plugins/primeares_tests.smx
3. Remove .../left4dead2/addons/sourcemod/scripting/include/primeares.inc
4. Remove .../left4dead2/addons/sourcemod/data/primeares.dat
5. Remove .../left4dead2/cfg/sourcemod/primeares.cfg

### Disabling PrimeAres

> Caution: Some plugins may require PrimeAres.

1. Move primeares.smx into plugins/disabled
2. Move primeares_tests.smx into plugins/disabled
3. ```sm_rcon sm plugins unload primeares```

## Configuration Variables (Cvars)
```
"pa_debug" = "0"
 - 0: Disables weapon testing (default) 1: Reloading a primary weapon will give that weapon 1 ammo 2: Will constantly print out weapon information
"pa_echo" = "0"
 - Higher values increase the verbosity on calls PrimeAres makes.
 ```

## Admin Chat Commands
```
!pa_test
- This will test PrimeAres against the caller (requires admin generic flag)
```

## Customizing PrimeAres (User Example)

> Note: Create a primeares.dat file and move to .../left4dead2/addons/sourcemod/data/

```
PrimeAres {
    weapon_rifle_m60 {
        reserve 850
        upbit 4
    }

    weapon_rifle {
        upbit 4
    }

	weapon_rifle_ak47
		clip 40
		upbit 6
		upclip 40
	}
}
```

### Customing Explained
The main key needs to be ```PrimeAres```. Sub keys have to be a valid weapon name (you can get a list of them with ```sm_dump_classes```). Every sub key opens up with a ```{``` and needs to be closed with a ```}```. Valid Sub key keys are ```clip, type, reserve, upbit, upclip```. All key values apply to weapons picked up from a spawn. Only clip and reserve are considered at ammo piles. None are applied to weapons previously dropped.

> Note: You can mess with clip but you're on your own. Also, due to limitations, clips larger than 256 are truncated to 255.

#### Upbit Values
If you set upbit to support either incendiary or explosive, make sure you also add ```clip``` and ```upclip``` values that mirror the other e.g., 40 and 40 or 10 and 10. Not 10 and 50 or 50 and 0.
- ```1``` Incendiary
- ```2``` Explosive
- ```4``` Lasers
- ```5``` Lasers + Incendiary

## For Developers
PrimeAres exposes some natives to work with primaries. To begin, you'll need ```#include <primeares>``` in the header of your source file. You'll also need to have installed the ```primeares.inc``` in your ```include``` folder (where you develop) and ```primeares.smx``` running on the server.

```
paGetDefClip
paGetDefType
paGetDefReserve
paGetDefUpgrades
paGetDefUpgradeClip
paGetDefaults
paGetWeaponFeatures
paSetWeaponFeatures
paGetClip
paGetType
paGetReserve
paGetUpgrades
paGetUpgradeClip
paSetClip
paSetType
paSetReserve
paSetUpgrades
paSetUpgradeClip
paHasLaser
paHasExplosive
paHasIncendiary
paSetLaser
paSetExplosive
paSetIncendiary
paDefrostWeapon
paFreezeWeapon
paCheckAndFreeze
paIsEntityUpgradeAmmo
paCanUseUpgradeAmmo
paIsWeapon
paIsEmpty
paIsFrozen
paPrintWeapon
```
Most of these are self explanatory but if in doubt, see ```primeares.inc``` and ```primeares_tests.sp``` for more information and usage.

## Thanks
Big thanks to **Timocop** and **Lux** for allowing me to break down your ammo stock and especially to Timocop for taking the time to break down the meaning of '256'. Big thanks to Peace-Maker for ever posting this [snippet](https://forums.alliedmods.net/showpost.php?p=2462798&postcount=4). <3

## Reaching Me
I love L4D2, developing, testing and running servers more than I like playing the game. Although I do enjoy the game and it is undoubtedly my favorite game, it is the community I think I love the most. It's always good to meet new people with the same interest :)

- [My Steam Profile](http://steamcommunity.com/id/buckwangs/)
- [My PrimeAres GitLab Page](https://gitlab.com/vbgunz/PrimeAres)

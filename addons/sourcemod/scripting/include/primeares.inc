 #if defined _primeares_included
	#endinput
#endif
#define _primeares_included


/**
 * Get default/keyValue clip size of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo clip size
 */
native int paGetDefClip(int entity);


/**
 * Get default/keyValue ammo type of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo type
 */
native int paGetDefType(int entity);


/**
 * Get default/keyValue reserve ammo of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo reserve
 */
native int paGetDefReserve(int entity);


/**
 * Get default/keyValue upgrade features of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo clip size
 */
native int paGetDefUpgrades(int entity);


/**
 * Get default/keyValue upgrade clip size of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo clip size
 */
native int paGetDefUpgradeClip(int entity);


/**
 * Get default/keyValue weapon features
 *
 * @param ent       Entity of weapon
 * @param clip      Ammo clip size
 * @param type      Ammo type
 * @param reserve   Ammo reserve
 * @param upbit     Upgrade features
 * @param upclip    Upgrade ammo clip size
 * @return          True if we could stat the weapon, false otherwise
 */
native bool paGetDefaults(int ent, int &clip, int &type, int &reserve, int &upbit, int &upclip);


/**
 * Get a weapons features
 *
 * @param ent       Entity of weapon
 * @param clip      Ammo clip size
 * @param type      Ammo type
 * @param reserve   Ammo reserve
 * @param upbit     Upgrade features
 * @param upclip    Upgrade ammo clip size
 * @return          True if we could stat the weapon, false otherwise
 */
native bool paGetWeaponFeatures(int ent, int &clip, int &type, int &reserve, int &upbit, int &upclip);


/**
 * Set a weapons features
 *
 * @param ent       Entity of weapon
 * @param clip      Ammo clip size
 * @param type      Ammo type
 * @param reserve   Ammo reserve
 * @param upbit     Upgrade features
 * @param upclip    Upgrade ammo clip size
 * @return          void
 */
native void paSetWeaponFeatures(int ent, int clip, int type, int reserve, int upbit, int upclip);


/**
 * Get clip size of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo clip size
 */
native int paGetClip(int entity);


/**
 * Get ammo type of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo type
 */
native int paGetType(int entity);


/**
 * Get reserve ammo of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo reserve
 */
native int paGetReserve(int entity);


/**
 * Get upgrade features of weapon
 *
 * @param ent       Entity of weapon
 * @return          Upgrade features
 */
native int paGetUpgrades(int entity);


/**
 * Get upgrade clip size of weapon
 *
 * @param ent       Entity of weapon
 * @return          Ammo clip size
 */
native int paGetUpgradeClip(int entity);


/**
 * Set clip size of weapon
 *
 * @param ent       Entity of weapon
 * @return          void
 */
native void paSetClip(int entity, int value);


/**
 * Set ammo type of weapon
 *
 * @param ent       Entity of weapon
 * @param value     Ammo type
 * @return          void
 */
native void paSetType(int entity, int value);


/**
 * Set reserve ammo of weapon
 *
 * @param ent       Entity of weapon
 * @param value     Ammo reserve
 * @return          void
 */
native void paSetReserve(int entity, int value);


/**
 * Set upgrade features of weapon
 *
 * @param ent       Entity of weapon
 * @param value     Upgrade features
 * @return          void
 */
native void paSetUpgrades(int entity, int value);


/**
 * Set upgrade clip size of weapon
 *
 * @param ent       Entity of weapon
 * @param value     Upgrade ammo clip size
 * @return          void
 */
native void paSetUpgradeClip(int entity, int value);


/**
 * Check if the weapon has a laser sight
 *
 * @param ent       Entity of weapon
 * @return          True if weapon has a laser sight, false otherwise
 */
native bool paHasLaser(int entity);


/**
 * Check if the weapon has explosive ammo
 *
 * @param ent       Entity of weapon
 * @return          True if weapon has explosive ammo, false otherwise
 */
native bool paHasExplosive(int entity);


/**
 * Check if the weapon has incendiary ammo
 *
 * @param ent       Entity of weapon
 * @return          True if weapon has incendiary ammo, false otherwise
 */
native bool paHasIncendiary(int entity);


/**
 * Set a laser sight on the weapon
 *
 * @param ent       Entity of weapon
 * @return          void
 */
native void paSetLaser(int entity);


/**
 * Set weapon with explosive ammo
 *
 * @param ent       Entity of weapon
 * @return          void
 */
native void paSetExplosive(int entity);


/**
 * Set weapon with incendiary ammo
 *
 * @param ent       Entity of weapon
 * @return          void
 */
native void paSetIncendiary(int entity);


/**
 * Set weapon to defrost (will require ammo to be useable)
 *
 * @param ent       Entity of weapon
 * @return          void
 */
native void paDefrostWeapon(int entity);


/**
 * Freeze a primary weapon
 *
 * @param ent       Entity of weapon
 * @return          void
 */
native void paFreezeWeapon(int entity);


/**
 * Check and if necessary freeze an empty weapon.
 *
 * @param ent       Entity of weapon
 * @return          True if weapon is frozen, false otherwise
 */
native bool paCheckAndFreeze(int entity);


/**
 * Checks if entity is an upgrade_ammo
 *
 * @param ent       Entity of upgrade_ammo
 * @return          True if entity is an upgrade_ammo, false otherwise
 */
native bool paIsEntityUpgradeAmmo(int ent);


/**
 * Checks if player can use an upgrade ammo entity
 *
 * @param client    Client to check
 * @param ent       Entity of upgrade
 * @return          True if player can upgrade, false otherwise
 */
native bool paCanUseUpgradeAmmo(int client, int ent);


/**
 * Check if the entity is a primary weapon
 *
 * @param ent       Entity of weapon
 * @return          True if entity is a primary weapon
 */
native bool paIsWeapon(int entity);


/**
 * Check if the weapon is empty
 *
 * @param ent       Entity of weapon
 * @return          True if entity is a primary weapon and empty, false otherwise
 */
native bool paIsEmpty(int entity);


/**
 * Check if the weapon has been frozen
 *
 * @param ent       Entity of weapon
 * @return          True if weapon is frozen, false otherwise
 */
native bool paIsFrozen(int entity);


/**
 * Print out weapon features
 *
 * @param ent       Entity of weapon
 * @param note      Optional prefix note for the print
 * @return          void
 */
native void paPrintWeapon(int ent, const char[] note="");


/**
 * Get the client behind the weapon
 *
 * @param ent       Entity of weapon
 * @return          Client Id
 */
native int paGetClient(int entity);

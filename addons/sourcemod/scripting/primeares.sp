//# vim: set filetype=cpp :

/*
 * license = "https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html#SEC1",
 */

#pragma semicolon 1
//#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#define PLUGIN_NAME "PrimeAres"
#define PLUGIN_VERSION "0.0.5"
#define KEYLEN 64

char g_sB[512];
ConVar g_cvEcho;
ConVar g_cvDebug;
Handle g_kv;

int m60s[MAXPLAYERS + 1];
float pauseSwitch[MAXPLAYERS + 1];
int upgradesUsed[2049][MAXPLAYERS + 1];
int snapshots[2049][6];  // ent, clip, type, reserve, upbit, upclip
int stats[2049][6];  // ent, clip, type, reserve, upbit, upclip
int g_holding[MAXPLAYERS + 1];  // see SDKUseHook && CheckPickUp

public Plugin myinfo= {
    name = PLUGIN_NAME,
    author = "Victor \"NgBUCKWANGS\" Gonzalez",
    description = "An L4D2 Primary Weapons Library",
    version = PLUGIN_VERSION,
    url = "https://gitlab.com/vbgunz/PrimeAres"
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] err, int err_max) {
    RegPluginLibrary("primeares");

    CreateNative("paGetDefClip", paGetDefClip);
    CreateNative("paGetDefType", paGetDefType);
    CreateNative("paGetDefReserve", paGetDefReserve);
    CreateNative("paGetDefUpgrades", paGetDefUpgrades);
    CreateNative("paGetDefUpgradeClip", paGetDefUpgradeClip);
    CreateNative("paGetDefaults", paGetDefaults);
    CreateNative("paGetWeaponFeatures", paGetWeaponFeatures);
    CreateNative("paSetWeaponFeatures", paSetWeaponFeatures);
    CreateNative("paGetClip", paGetClip);
    CreateNative("paGetType", paGetType);
    CreateNative("paGetReserve", paGetReserve);
    CreateNative("paGetUpgrades", paGetUpgrades);
    CreateNative("paGetUpgradeClip", paGetUpgradeClip);
    CreateNative("paSetClip", paSetClip);
    CreateNative("paSetType", paSetType);
    CreateNative("paSetReserve", paSetReserve);
    CreateNative("paSetUpgrades", paSetUpgrades);
    CreateNative("paSetUpgradeClip", paSetUpgradeClip);
    CreateNative("paHasLaser", paHasLaser);
    CreateNative("paHasExplosive", paHasExplosive);
    CreateNative("paHasIncendiary", paHasIncendiary);
    CreateNative("paSetLaser", paSetLaser);
    CreateNative("paSetExplosive", paSetExplosive);
    CreateNative("paSetIncendiary", paSetIncendiary);
    CreateNative("paDefrostWeapon", paDefrostWeapon);
    CreateNative("paFreezeWeapon", paFreezeWeapon);
    CreateNative("paCheckAndFreeze", paCheckAndFreeze);
    CreateNative("paIsEntityUpgradeAmmo", paIsEntityUpgradeAmmo);
    CreateNative("paCanUseUpgradeAmmo", paCanUseUpgradeAmmo);
    CreateNative("paIsWeapon", paIsWeapon);
    CreateNative("paIsEmpty", paIsEmpty);
    CreateNative("paIsFrozen", paIsFrozen);
    CreateNative("paPrintWeapon", paPrintWeapon);
    CreateNative("paGetClient", paGetClient);
}

public OnPluginStart() {
    HookEvent("player_disconnect", OnPlayerDisconnectHook);

    g_cvEcho = CreateConVar("pa_echo", "0",
        "Higher values increase the verbosity on calls PrimeAres makes."
    );

    g_cvDebug = CreateConVar("pa_debug", "0",
        "0: Disables weapon testing (default) \
         1: Reloading a primary weapon will give that weapon 1 ammo \
         2: Will constantly print out weapon information"
    );

    //AutoExecConfig(true, "primeares");
}

public OnMapStart() {
    ReloadData();

    for (int i = MAXPLAYERS; i <= 2048; i++) {

        // clear snapshots
        for (int j = 0; j < sizeof(snapshots[]); j++) {
            snapshots[i][j] = 0;
        }

        // clear upgradesUsed
        for (int j = 0; j < sizeof(upgradesUsed[]); j++) {
            upgradesUsed[i][j] = 0;
        }

        // clear weapon stats
        for (int j = 0; j < sizeof(stats[]); j++) {
            stats[i][j] = 0;
        }

        // rehook entities (sm plugins reload primeares)
        if (IsEntityValid(i)) {
            GetEntityClassname(i, g_sB, sizeof(g_sB));
            OnEntityCreated(i, g_sB);
        }
    }

    // manually run through clients for cleanup and hooking
    for (int i = 1; i <= MaxClients; i++) {
        m60s[i] = 0;
        pauseSwitch[i] = 0.0;
        OnClientPostAdminCheck(i);
    }
}

public void OnEntityCreated(int ent, const char[] clsName) {
    Echo(5, "OnEntityCreated: %d '%s'", ent, clsName);
    SDKHook(ent, SDKHook_Use, SDKUseHook);
    ResetUpgrade(ent);
}

public void OnEntityDestroyed(int ent) {
    Echo(5, "OnEntityDestroyed: %d", ent);
    SDKUnhook(ent, SDKHook_Use, SDKUseHook);
    ResetUpgrade(ent);
}

public OnClientPostAdminCheck(int client) {
    Echo(2, "OnClientPostAdminCheck: %d", client);

    if (IsClientValid(client, 0, 1)) {
        SDKHook(client, SDKHook_WeaponSwitchPost, SDKWeaponSwitchPostHook);
        SDKHook(client, SDKHook_WeaponCanSwitchTo, SDKWeaponCanSwitchToHook);
        SDKHook(client, SDKHook_WeaponDrop, SDKWeaponDropHook);
    }
}

public OnPlayerDisconnectHook(Handle event, const char[] name, bool dontBroadcast) {
    Echo(2, "OnPlayerDisconnectHook: '%s'", name);

    int userid = GetEventInt(event, "userid");
    int client = GetClientOfUserId(userid);

    if (IsClientValid(client, 0, 1)) {
        SDKUnhook(client, SDKHook_WeaponSwitchPost, SDKWeaponSwitchPostHook);
        SDKUnhook(client, SDKHook_WeaponCanSwitchTo, SDKWeaponCanSwitchToHook);
        SDKUnhook(client, SDKHook_WeaponDrop, SDKWeaponDropHook);
    }
}

public Action SDKUseHook(int entity, int client, int caller, UseType uType, float value) {
    Echo(2, "SDKUseHook: %d %d %d - %d", entity, client, caller, value);

    if (!IsClientValid(client) || !IsEntityValid(entity)) {
        return Plugin_Continue;
    }

    int weapon = GetPlayerWeaponSlot(client, 0);

    static int clip, type, reserve, upbit, upclip;
    if (!GetDefaults(weapon, clip, type, reserve, upbit, upclip)) {
        return Plugin_Continue;
    }

    static char entityName[KEYLEN], weaponName[KEYLEN];
    GetEntityClassname(entity, entityName, sizeof(entityName));
    GetEntityClassname(weapon, weaponName, sizeof(weaponName));
    Echo(1, "USING: '%s'", entityName);

    static int ammo;
    ammo = 0;

    if (entityName[0] == 'u') {
        if (StrContains(entityName, "upgrade_ammo") == 0) {
            if (!CanUseUpgradeAmmo(client, entity)) {
                return Plugin_Handled;
            }

            upgradesUsed[entity][client] = 1;
            switch (StrContains(entityName, "incendiary") == 13) {
                case 1: ammo = 1;
                case 0: ammo = 2;
            }
        }
    }

    else if (entityName[0] == 'w') {
        if (StrContains(entityName, "weapon_ammo") == 0) {
            ammo = 3;
        }

        else if (StrContains(entityName, "_spawn") >= 0) {
            g_holding[client] = weapon;
            RequestFrame(CheckPickUp, client);
            return Plugin_Continue;
        }
    }

    if (ammo == 0) {
        Echo(1, "UNHOOKd: '%s'", entityName);
        SDKUnhook(entity, SDKHook_Use, SDKUseHook);
        return Plugin_Continue;
    }

    DefrostWeapon(weapon);

    switch (ammo) {
        case 1, 2: {  // picking up special ammo

            switch (ammo) {
                case 1: _SetIncendiary(weapon);
                case 2: _SetExplosive(weapon);
            }

            _SetClip(weapon, clip);
            _SetUpgradeClip(weapon, clip);
        }

        case 3: {  // picking up regular ammo
            reserve = (reserve + clip) - _GetClip(weapon);
            _SetReserve(weapon, reserve);
        }
    }

    return Plugin_Handled;
}

public Action SDKWeaponSwitchPostHook(int client, int weapon) {
    Echo(2, "SDKWeaponSwitchPostHook: %d %d", client, weapon);

    if (IsClientValid(client, 2, 1)) {
        static int ent;
        ent = GetPlayerWeaponSlot(client, 0);

        if (IsEntityValid(ent) && ent != weapon) {
            CheckAndFreeze(ent);
        }
    }
}

public Action SDKWeaponCanSwitchToHook(int client, int weapon) {
    Echo(2, "SDKWeaponCanSwitchToHook: %d %d", client, weapon);

    // mostly to keep the m60 acting the same
    if (pauseSwitch[client] >= GetGameTime()) {
        EquipSecondary(client);
        return Plugin_Handled;
    }

    return Plugin_Continue;
}

public Action SDKWeaponDropHook(int client, int ent) {
    Echo(2, "SDKWeaponDropHook: %d %d", client, ent);

    if (IsClientValid(client, 2, 1) && IsEntityValid(ent)) {
        static char clsName[KEYLEN];
        GetEntityClassname(ent, clsName, sizeof(clsName));

        if (clsName[0] == 'w' && StrEqual(clsName, "weapon_rifle_m60")) {

            // workaround to avoid a good yet dead M60 on the floor
            if (GetEntProp(ent, Prop_Data, "m_bInReload")) {
                _SetClip(ent, 1); _SetReserve(ent, _GetReserve(ent) - 1);
            }

            if (IsEmpty(ent)) {
                pauseSwitch[client] = GetGameTime() + 0.25;
            }

            if (SnapshotWeapon(ent)) {
                m60s[client] = ent;
                RequestFrame(ReplM60, client);
            }
        }
    }
}

/*
// NATIVES BEGIN
*/

public paGetDefClip(Handle plugin, params) {  // NATIVE
    return _GetDefClip(GetNativeCell(1));
}

int _GetDefClip(int ent) {
    Echo(3, "_GetDefClip: %d", ent);
    return StatWeapon(ent) ? stats[ent][1] : -1;
}

public paGetDefType(Handle plugin, params) {  // NATIVE
    return _GetDefType(GetNativeCell(1));
}

int _GetDefType(int ent) {
    Echo(3, "_GetDefType: %d", ent);
    return StatWeapon(ent) ? stats[ent][2] : -1;
}

public paGetDefReserve(Handle plugin, params) {  // NATIVE
    return _GetDefReserve(GetNativeCell(1));
}

int _GetDefReserve(int ent) {
    Echo(3, "_GetDefReserve: %d", ent);
    return StatWeapon(ent) ? stats[ent][3] : -1;
}

public paGetDefUpgrades(Handle plugin, params) {  // NATIVE
    return _GetDefUpgrades(GetNativeCell(1));
}

int _GetDefUpgrades(int ent) {
    Echo(3, "_GetDefUpgrades: %d", ent);
    return StatWeapon(ent) ? stats[ent][4] : -1;
}

public paGetDefUpgradeClip(Handle plugin, params) {  // NATIVE
    return _GetDefUpgradeClip(GetNativeCell(1));
}

int _GetDefUpgradeClip(int ent) {
    Echo(3, "_GetDefUpgradeClip: %d", ent);
    return StatWeapon(ent) ? stats[ent][5] : -1;
}

public paGetDefaults(Handle plugin, params) {  // NATIVE
    static int result, clip, type, reserve, upbit, upclip;
    result = GetDefaults(GetNativeCell(1), clip, type, reserve, upbit, upclip);
    SetNativeCellRef(2, clip);
    SetNativeCellRef(3, type);
    SetNativeCellRef(4, reserve);
    SetNativeCellRef(5, upbit);
    SetNativeCellRef(6, upclip);
    return result;
}

bool GetDefaults(int ent, int &clip, int &type, int &reserve, int &upbit, int &upclip) {
    Echo(2, "GetDefaults: %d %d %d %d %d %d", ent, clip, type, reserve, upbit, upclip);

    clip = type = reserve = upbit = upclip = -1;

    if (StatWeapon(ent)) {
        clip = stats[ent][1];
        type = stats[ent][2];
        reserve = stats[ent][3];
        upbit = stats[ent][4];
        upclip = stats[ent][5];
        return true;
    }

    return false;
}

public paGetWeaponFeatures(Handle plugin, params) {  // NATIVE
    static int result, clip, type, reserve, upbit, upclip;
    result = GetWeaponFeatures(GetNativeCell(1), clip, type, reserve, upbit, upclip);
    SetNativeCellRef(2, clip);
    SetNativeCellRef(3, type);
    SetNativeCellRef(4, reserve);
    SetNativeCellRef(5, upbit);
    SetNativeCellRef(6, upclip);
    return result;
}

bool GetWeaponFeatures(int ent, int &clip, int &type, int &reserve, int &upbit, int &upclip) {
    Echo(2, "GetWeaponFeatures: %d %d %d %d %d %d", ent, clip, type, reserve, upbit, upclip);

    clip = type = reserve = upbit = upclip = -1;

    if (IsEntityValid(ent)) {
        clip = _GetClip(ent);
        type = _GetType(ent);
        reserve = _GetReserve(ent);
        upbit = _GetUpgrades(ent);
        upclip = _GetUpgradeClip(ent);
        return true;
    }

    return false;
}

public paSetWeaponFeatures(Handle plugin, params) {  // NATIVE
    SetWeaponFeatures(
        GetNativeCell(1),
        GetNativeCell(2),
        GetNativeCell(3),
        GetNativeCell(4),
        GetNativeCell(5),
        GetNativeCell(6)
    );
}

void SetWeaponFeatures(int ent, int clip, int type, int reserve, int upbit, int upclip) {
    Echo(2, "SetWeaponFeatures: %d %d %d %d %d %d", ent, clip, type, reserve, upbit, upclip);

    if (IsEntityValid(ent)) {
        _SetClip(ent, clip);
        _SetType(ent, type);
        _SetReserve(ent, reserve);
        _SetUpgrades(ent, upbit);
        _SetUpgradeClip(ent, upclip);
    }
}

public paGetClip(Handle plugin, params) {  // NATIVE
    return _GetClip(GetNativeCell(1));
}

int _GetClip(int ent) {
    Echo(3, "_GetClip: %d", ent);
    return GetEntProp(ent, Prop_Send, "m_iClip1");
}

public paGetType(Handle plugin, params) {  // NATIVE
    return _GetType(GetNativeCell(1));
}

int _GetType(int ent) {
    Echo(3, "_GetType: %d", ent);
    return GetEntProp(ent, Prop_Send, "m_iPrimaryAmmoType");
}

public paGetReserve(Handle plugin, params) {  // NATIVE
    return _GetReserve(GetNativeCell(1));
}

int _GetReserve(int ent) {
    Echo(3, "_GetReserve: %d", ent);

    static int client;
    client = _GetClient(ent);

    if (IsClientValid(client, 2)) {
        return GetEntProp(client, Prop_Send, "m_iAmmo", _, _GetType(ent));
    }

    return -1;
}

public paGetUpgrades(Handle plugin, params) {  // NATIVE
    return _GetUpgrades(GetNativeCell(1));
}

int _GetUpgrades(int ent) {
    Echo(3, "_GetUpgrades: %d", ent);
    return GetEntProp(ent, Prop_Send, "m_upgradeBitVec");
}

public paGetUpgradeClip(Handle plugin, params) {  // NATIVE
    return _GetUpgradeClip(GetNativeCell(1));
}

int _GetUpgradeClip(int ent) {
    Echo(3, "_GetUpgradeClip: %d", ent);
    return GetEntProp(ent, Prop_Send, "m_nUpgradedPrimaryAmmoLoaded");
}

public paSetClip(Handle plugin, params) {  // NATIVE
    _SetClip(GetNativeCell(1), GetNativeCell(2));
}

void _SetClip(int ent, int value) {
    Echo(3, "_SetClip: %d %d", ent, value);
    SetEntProp(ent, Prop_Send, "m_iClip1", value);
}

public paSetType(Handle plugin, params) {  // NATIVE
    _SetType(GetNativeCell(1), GetNativeCell(2));
}

void _SetType(int ent, int value) {
    Echo(3, "_SetType: %d %d", ent, value);
    SetEntProp(ent, Prop_Send, "m_iPrimaryAmmoType", value);
}

public paSetReserve(Handle plugin, params) {  // NATIVE
    _SetReserve(GetNativeCell(1), GetNativeCell(2));
}

void _SetReserve(int ent, int value) {
    Echo(3, "_SetReserve: %d %d", ent, value);

    static int client;
    client = _GetClient(ent);

    if (IsClientValid(client, 2)) {
        SetEntProp(client, Prop_Send, "m_iAmmo", value, _, _GetType(ent));
    }
}

public paSetUpgrades(Handle plugin, params) {  // NATIVE
    _SetUpgrades(GetNativeCell(1), GetNativeCell(2));
}

void _SetUpgrades(int ent, int value) {
    Echo(3, "_SetUpgrades: %d %d", ent, value);
    SetEntProp(ent, Prop_Send, "m_upgradeBitVec", value);
}

public paSetUpgradeClip(Handle plugin, params) {  // NATIVE
    _SetUpgradeClip(GetNativeCell(1), GetNativeCell(2));
}

void _SetUpgradeClip(int ent, int value) {
    Echo(3, "_SetUpgradeClip: %d %d", ent, value);
    SetEntProp(ent, Prop_Send, "m_nUpgradedPrimaryAmmoLoaded", value);
}

public paHasLaser(Handle plugin, params) {  // NATIVE
    return _HasLaser(GetNativeCell(1));
}

bool _HasLaser(int ent) {
    Echo(3, "_HasLaser: %d", ent);
    if (_GetUpgrades(ent) & (1 << 2))
        return true;
    return false;
}

public paHasExplosive(Handle plugin, params) {  // NATIVE
    return _HasExplosive(GetNativeCell(1));
}

bool _HasExplosive(int ent) {
    Echo(3, "_HasExplosive: %d", ent);
    if (_GetUpgrades(ent) & (1 << 1))
        return true;
    return false;
}

public paHasIncendiary(Handle plugin, params) {  // NATIVE
    return _HasIncendiary(GetNativeCell(1));
}

bool _HasIncendiary(int ent) {
    Echo(3, "_HasIncendiary: %d", ent);
    if (_GetUpgrades(ent) & (1 << 0))
        return true;
    return false;
}

public paSetLaser(Handle plugin, params) {  // NATIVE
    _SetLaser(GetNativeCell(1));
}

void _SetLaser(int ent) {
    Echo(3, "_SetLaser: %d", ent);

    if (!(_HasLaser(ent))) {
        _SetUpgrades(ent, _GetUpgrades(ent) + 4);
    }
}

public paSetExplosive(Handle plugin, params) {  // NATIVE
    _SetExplosive(GetNativeCell(1));
}

void _SetExplosive(int ent) {
    Echo(3, "_SetExplosive: %d", ent);

    static int upbit;
    if (_HasLaser(ent)) {
        upbit = 4;
    }

    _SetUpgrades(ent, upbit + 2);
    _SetUpgradeClip(ent, _GetClip(ent));
}

public paSetIncendiary(Handle plugin, params) {  // NATIVE
    _SetIncendiary(GetNativeCell(1));
}

void _SetIncendiary(int ent) {
    Echo(3, "_SetIncendiary: %d", ent);

    static int upbit;
    if (_HasLaser(ent)) {
        upbit = 4;
    }

    _SetUpgrades(ent, upbit + 1);
    _SetUpgradeClip(ent, _GetClip(ent));
}

public paDefrostWeapon(Handle plugin, params) {  // NATIVE
    DefrostWeapon(GetNativeCell(1));
}

void DefrostWeapon(int ent) {
    Echo(2, "DefrostWeapon: %d", ent);

    if (IsFrozen(ent)) {
        static int upbit;
        upbit = 0;
        if (_HasLaser(ent)) {
            upbit = 4;
        }

        SetWeaponFeatures(ent, 0, _GetType(ent), 0, upbit, 0);
    }
}

public paFreezeWeapon(Handle plugin, params) {  // NATIVE
    FreezeWeapon(GetNativeCell(1));
}

void FreezeWeapon(int ent) {
    Echo(2, "FreezeWeapon: %d", ent);

    if (IsEntityValid(ent)) {
        static int upbit;
        upbit = 0;
        if (_HasLaser(ent)) {
            upbit += 4;
        }

        SetWeaponFeatures(ent, 1, _GetType(ent), 0, upbit, 1);
    }
}

public paCheckAndFreeze(Handle plugin, params) {  // NATIVE
    return CheckAndFreeze(GetNativeCell(1));
}

bool CheckAndFreeze(int ent) {
    Echo(2, "CheckAndFreeze: %d", ent);

    if (IsWeapon(ent) && IsEmpty(ent)) {
        FreezeWeapon(ent);
        return true;
    }

    return false;
}

public paIsEntityUpgradeAmmo(Handle plugin, params) {  // NATIVE
    return IsEntityUpgradeAmmo(GetNativeCell(1));
}

bool IsEntityUpgradeAmmo(int ent) {
    Echo(2, "IsEntityUpgradeAmmo: %d", ent);

    static char clsName[KEYLEN];
    GetEntityClassname(ent, clsName, sizeof(clsName));

    if (clsName[0] == 'u' && StrContains(clsName, "upgrade_ammo") == 0) {
        Echo(1, "PROCESSING: %d '%s'", ent, clsName);
        return true;
    }

    return false;
}

public paCanUseUpgradeAmmo(Handle plugin, params) {  // NATIVE
    return CanUseUpgradeAmmo(GetNativeCell(1), GetNativeCell(2));
}

bool CanUseUpgradeAmmo(int client, int ent) {
    Echo(2, "CanUseUpgradeAmmo: %d", ent);
    return IsEntityUpgradeAmmo(ent) && upgradesUsed[ent][client] == 0;
}

public paIsWeapon(Handle plugin, params) {  // NATIVE
    return IsWeapon(GetNativeCell(1));
}

bool IsWeapon(int ent) {
    Echo(2, "IsWeapon: %d", ent);

    if (IsEntityValid(ent) && HasEntProp(ent, Prop_Data, "m_iClip1")
        && HasEntProp(ent, Prop_Send, "m_iPrimaryAmmoType")
        && HasEntProp(ent, Prop_Send, "m_upgradeBitVec")
        && HasEntProp(ent, Prop_Send, "m_nUpgradedPrimaryAmmoLoaded")) {
            return true;
    }

    return false;
}

public paIsEmpty(Handle plugin, params) {  // NATIVE
    return IsEmpty(GetNativeCell(1));
}

bool IsEmpty(int ent) {
    Echo(2, "IsEmpty: %d", ent);

    return (IsFrozen(ent) || IsWeapon(ent)
        && _GetClip(ent) == 0 && _GetReserve(ent) == 0
    );
}

public paIsFrozen(Handle plugin, params) {  // NATIVE
    return IsFrozen(GetNativeCell(1));
}

bool IsFrozen(int ent) {
    Echo(2, "IsFrozen: %d", ent);

    return (IsWeapon(ent) && _GetClip(ent) == _GetUpgradeClip(ent)
        && _GetReserve(ent) == 0 && !_HasExplosive(ent) && !_HasIncendiary(ent)
    );
}

public paPrintWeapon(Handle plugin, params) {  // NATIVE
    static char b[KEYLEN];
    if (SP_ERROR_NONE == GetNativeString(2, b, sizeof(b))) {
        PrintWeapon(GetNativeCell(1), b);
    }
}

void PrintWeapon(int ent, const char[] note="") {
    Echo(2, "PrintWeapon: %d '%s'", ent, note);

    static char entityName[KEYLEN];
    entityName = "INVALID: ENTITY";

    if (IsEntityValid(ent)) {
        GetEntityClassname(ent, entityName, sizeof(entityName));
    }

    if (IsWeapon(ent)) {
        static int clip, type, reserve, upbit, upclip;
        GetWeaponFeatures(ent, clip, type, reserve, upbit, upclip);

        PrintToServer(
            "%s: name %s, ent %d, clip %d, type %d, reserve %d, upbit %d, upclip %d",\
            note, entityName, ent, clip, type, reserve, upbit, upclip);
    }

    else {
        PrintToServer("NOT A WEAPON: '%s'", entityName);
    }
}

public paGetClient(Handle plugin, params) {  // NATIVE
    return _GetClient(GetNativeCell(1));
}

int _GetClient(int ent) {
    Echo(3, "_GetClient: %d", ent);
    return GetEntPropEnt(ent, Prop_Send, "m_hOwnerEntity");
}

/*
// NATIVES END
*/

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon) {
    Echo(5, "OnPlayerRunCmd: %d %d %d %d %d %d", client, buttons, impulse, vel, angles, weapon);

    if (IsClientValid(client, 2, 1)) {
        static int ent, clip, type, reserve, upbit, upclip;
        ent = GetPlayerWeaponSlot(client, 0);

        if (IsFrozen(ent)) {
            SetEntPropFloat(
                ent, Prop_Send, "m_flNextPrimaryAttack", GetGameTime() + 1.0
            );
        }

        if (GetConVarInt(g_cvDebug)) {  // FOR TESTING ONLY
            GetWeaponFeatures(ent, clip, type, reserve, upbit, upclip);
            static char msg[64] = "ent %d clip %d type %d reserve %d upbit %d upclip %d";

            if (GetConVarInt(g_cvDebug) == 2) {
                PrintToServer(msg, ent, clip, type, reserve, upbit, upclip);
            }

            if (buttons == 8192 && IsEntityValid(ent)) {
                upbit = 0;
                if (_HasLaser(ent)) {
                    upbit = 4;
                }

                SetWeaponFeatures(ent, 1, _GetType(ent), 0, upbit, 0);
            }
        }
    }
}

void Echo(int level, char [] format, any ...) {
    static char g_dB[512];

    if (GetConVarInt(g_cvEcho) >= level) {
        VFormat(g_dB, sizeof(g_dB), format, 3);
        PrintToServer("%s", g_dB);
    }
}

bool IsEntityValid(int ent) {
    Echo(5, "IsEntityValid: %d", ent);
    return (ent > MaxClients && ent <= 2048 && IsValidEntity(ent));
}

bool IsClientValid(int client, int onteam=0, int mtype=2) {
    Echo(5, "IsClientValid: %d, %d, %d", client, onteam, mtype);

    if (client >= 1 && client <= MaxClients) {
        if (IsClientConnected(client) && IsClientInGame(client)) {
            if (onteam != 0 && GetClientTeam(client) != onteam) {
                return false;
            }

            switch (mtype) {
                case 0: return IsFakeClient(client);
                case 1: return !IsFakeClient(client);
            }

            return true;
        }
    }

    return false;
}

bool SnapshotWeapon(int ent) {
    Echo(2, "SnapshotWeapon: %d", ent);

    if (IsEntityValid(ent)) {
        CheckAndFreeze(ent);
        static int clip, type, reserve, upbit, upclip;
        GetWeaponFeatures(ent, clip, type, reserve, upbit, upclip);

        snapshots[ent][0] = EntIndexToEntRef(ent);
        snapshots[ent][1] = clip;
        snapshots[ent][2] = type;
        snapshots[ent][3] = reserve;
        snapshots[ent][4] = upbit;
        snapshots[ent][5] = upclip;

        for (int i = 1; i <= 5; i++) {
            if (snapshots[ent][i] < 0) {
                snapshots[ent][i] = 0;
            }
        }

        return true;
    }

    return false;
}

void AssignSnapshot(int index, int ent) {
    Echo(2, "AssignSnapshot: %d %d", index, ent);

    if (IsEntityValid(ent)) {
        SetWeaponFeatures(
            ent,
            snapshots[index][1], // clip
            snapshots[index][2], // type
            snapshots[index][3], // reserve
            snapshots[index][4], // upbit
            snapshots[index][5]  // upclip
        );
    }
}

void ClearSnapshot(int index) {
    Echo(2, "ClearSnapshot: %d", index);

    for (int i = 0; i <= 5; i++) {
        snapshots[index][i] = 0;
    }
}

void ReplM60(int client) {
    Echo(2, "ReplM60: %d", client);

    static int ent;
    ent = m60s[client];

    if (!IsEntityValid(GetPlayerWeaponSlot(client, 0))) {
        if (IsEntityValid(ent)) {
            if (client == _GetClient(ent)) {
                RemovePlayerItem(client, ent);
            }

            AcceptEntityInput(ent,"Kill");
        }

        int m60 = CreateEntityByName("weapon_rifle_m60");
        if (IsEntityValid(m60) && DispatchSpawn(m60)) {
            EquipPlayerWeapon(client, m60);
            AssignSnapshot(ent, m60);
        }
    }

    ClearSnapshot(ent);
}

void ReloadData(bool reload=false) {
    Echo(2, "ReloadData: %d", reload);

    if (!reload && g_kv != null) {
        return;
    }

    if (g_kv != null) {
        delete g_kv;
    }

    static char dataPath[PLATFORM_MAX_PATH];
    BuildPath(Path_SM, dataPath, sizeof(dataPath), "%s", "data/primeares.dat");
    g_kv = CreateKeyValues("PrimeAres");

    if (FileExists(dataPath)) {
        FileToKeyValues(g_kv, dataPath);
    }
}

void EquipSecondary(int client) {
    Echo(2, "EquipSecondary: %d", client);

    if (IsClientValid(client, 2, 1)) {
        int ent = GetPlayerWeaponSlot(client, 1);
        if (IsEntityValid(ent)) {
            SetEntPropEnt(client, Prop_Data, "m_hActiveWeapon", ent);
        }
    }
}

bool StatWeapon(int ent) {
    Echo(2, "StatWeapon: %d", ent);

    if (IsWeapon(ent)) {
        return StatClient(_GetClient(ent));
    }

    return false;
}

bool StatClient(int client) {
    Echo(2, "StatClient: %d", client);

    if (!IsClientValid(client, 2, 1)) {
        return false;
    }

    static int ent, origin;
    ent = GetPlayerWeaponSlot(client, 0);

    if (!IsWeapon(ent)) {
        return false;
    }

    static char clsName[KEYLEN];
    GetEntityClassname(ent, clsName, sizeof(clsName));

    if (ent == EntRefToEntIndex(stats[ent][0])) {

        Echo(1,
            "STAT: 1: %s, ent %d, clip %d, type %d, reserve %d, upbit %d, upclip %d",\
            clsName, ent, stats[ent][1], stats[ent][2], stats[ent][3], stats[ent][4], \
            stats[ent][5]
        );

        return true;
    }

    SnapshotWeapon(ent);
    SetWeaponFeatures(ent, 0, _GetType(ent), 0, 0, 0);

    static int ammo;
    ammo = FindEntityByClassname(-1, "weapon_ammo_spawn");
    GivePlayerAmmo(client, 9999999, _GetType(ent), false);  // jic
    if (IsEntityValid(ammo)) {  // this is what we really want
        SDKUnhook(ammo, SDKHook_Use, SDKUseHook);
        AcceptEntityInput(ammo, "Use", client);
        SDKHook(ammo, SDKHook_Use, SDKUseHook);
    }

    origin = CreateEntityByName(clsName);
    if (IsEntityValid(origin) && DispatchSpawn(origin)) {
        SetEntityRenderMode(origin, RENDER_NONE);

        static int reserve, maxAmmo;
        reserve = _GetReserve(ent);

        if (_GetClip(origin) <= reserve) {
            reserve -= _GetClip(origin);
        }

        // special exceptions everywhere!
        if (StrEqual(clsName, "weapon_grenade_launcher")) {
            reserve += _GetClip(origin);
        }

        KvJumpToKey(g_kv, clsName);
        stats[ent][0] = EntIndexToEntRef(ent);
        stats[ent][1] = KvGetNum(g_kv, "clip", _GetClip(origin));
        stats[ent][2] = KvGetNum(g_kv, "type", _GetType(origin));
        stats[ent][3] = KvGetNum(g_kv, "reserve", reserve);
        stats[ent][4] = KvGetNum(g_kv, "upbit", _GetUpgrades(origin));
        stats[ent][5] = KvGetNum(g_kv, "upclip", _GetUpgradeClip(origin));

        // set max ammo cvars (this needs to be generalized)
        // https://developer.valvesoftware.com/wiki/List_of_L4D2_Cvars
        maxAmmo = stats[ent][3];

        if (StrEqual(clsName, "weapon_grenade_launcher")) {
            SetConVarInt(FindConVar("ammo_grenadelauncher_max"), maxAmmo);
        }

        else if (StrEqual(clsName, "weapon_rifle_m60")) {
            SetConVarInt(FindConVar("ammo_m60_max"), maxAmmo);
        }

        KvRewind(g_kv);
        CheckAndFreeze(ent);

        Echo(1,
            "STAT: 0: %s, ent %d, clip %d, type %d, reserve %d, upbit %d, upclip %d",\
            clsName, ent, stats[ent][1], stats[ent][2], stats[ent][3], stats[ent][4], \
            stats[ent][5]
        );

        AcceptEntityInput(origin, "Kill");
        AssignSnapshot(ent, ent);
        ClearSnapshot(ent);
        return true;
    }

    return false;
}

void ResetUpgrade(int ent) {
    Echo(2, "ResetUpgrade: %d", ent);

    if (IsEntityValid(ent) && IsEntityUpgradeAmmo(ent)) {
        for (int i = 0; i <= MaxClients; i++) {
            upgradesUsed[ent][i] = 0;
        }
    }
}

void CheckPickUp(int client) {
    Echo(2, "CheckPickUp: %d", client);

    static int weapon;
    weapon = GetPlayerWeaponSlot(client, 0);

    if (weapon != g_holding[client]) {
        static int clip, type, reserve, upbit, upclip;
        if (GetDefaults(weapon, clip, type, reserve, upbit, upclip)) {
            SetWeaponFeatures(weapon, clip, type, reserve, upbit, upclip);
        }
    }

    g_holding[client] = -1;
}

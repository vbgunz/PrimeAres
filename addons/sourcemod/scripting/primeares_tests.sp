//# vim: set filetype=cpp :

/*
 * license = "https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html#SEC1",
 */

#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <primeares>

#define KEYLEN 64
#define PLUGIN_NAME "PrimeAresTestingPlugin"
#define PLUGIN_VERSION "0.0.1"

int _clip, _type, _reserve, _upbit, _upclip;
int tEnt, tclient, CLIP, TYPE, RESERVE, UPBIT, UPCLIP;

public Plugin myinfo= {
    name = PLUGIN_NAME,
    author = "Victor \"NgBUCKWANGS\" Gonzalez",
    description = "Unit Testing Plugin for PrimeAres",
    version = PLUGIN_VERSION,
    url = "https://gitlab.com/vbgunz/PrimeAres"
}

bool IsEntityValid(int ent) {
    return (ent > MaxClients && ent <= 2048 && IsValidEntity(ent));
}

bool IsClientValid(int client, int onteam=0, int mtype=2) {
    if (client >= 1 && client <= MaxClients) {
        if (IsClientConnected(client) && IsClientInGame(client)) {
            if (onteam != 0 && GetClientTeam(client) != onteam) {
                return false;
            }

            switch (mtype) {
                case 0: return IsFakeClient(client);
                case 1: return !IsFakeClient(client);
            }

            return true;
        }
    }

    return false;
}

public OnPluginStart() {
    RegAdminCmd("pa_test", Start, ADMFLAG_GENERIC);
}

void p(bool result, char [] format, any ...) {
    static char g_dB[512];
    VFormat(g_dB, sizeof(g_dB), format, 3);

    switch (result) {
        case 1: PrintToServer("PASS: %s", g_dB);
        case 0: PrintToServer("FAIL: %s", g_dB);
    }
}

// if you have modified the ak47, reflect that in the _Setup prototype...
void _Setup(int clip=40, int type=3, int reserve=360, int upbit=0, upclip=0) {

    _clip = _type = _reserve = _upbit = _upclip = -1;

    tEnt = GetPlayerWeaponSlot(tclient, 0);
    CLIP = clip;
    TYPE = type;
    RESERVE = reserve;
    UPBIT = upbit;
    UPCLIP = upclip;

    if (IsEntityValid(tEnt)) {
        RemovePlayerItem(tclient, tEnt);
        AcceptEntityInput(tEnt,"Kill");
    }

    tEnt = CreateEntityByName("weapon_rifle_ak47");
    if (IsEntityValid(tEnt) && DispatchSpawn(tEnt)) {
        AcceptEntityInput(tEnt, "Use", tclient);
        paSetWeaponFeatures(tEnt, clip, type, reserve, upbit, upclip);
    }
}

public Action Start(int client, args) {
    if (IsClientValid(client)) {
        tclient = client;

        paGetDefClip_test();
        paGetDefType_test();
        paGetDefReserve_test();
        paGetDefUpgrades_test();
        paGetDefUpgradeClip_test();
        paGetDefaults_test();
        paGetWeaponFeatures_test();
        paSetWeaponFeatures_test();
        paGetClip_test();
        paGetType_test();
        paGetReserve_test();
        paGetUpgrades_test();
        paGetUpgradeClip_test();
        paSetClip_test();
        paSetType_test();
        paSetReserve_test();
        paSetUpgrades_test();
        paSetUpgradeClip_test();
        paHasLaser_test();
        paHasExplosive_test();
        paHasIncendiary_test();
        paSetLaser_test();
        paSetExplosive_test();
        paSetIncendiary_test();
        paDefrostWeapon_test();
        paFreezeWeapon_test();
        paCheckAndFreeze_test();
        paIsEntityUpgradeAmmo_test();
        paCanUseUpgradeAmmo_test();
        paIsWeapon_test();
        paIsEmpty_test();
        paIsFrozen_test();
        paPrintWeapon_test();
        paGetClient_test();
    }
}

void paGetDefClip_test() {
    _Setup();
    p(paGetDefClip(tEnt) == CLIP, "paGetDefClip: %d", CLIP);
}

void paGetDefType_test() {
    _Setup();
    p(paGetDefType(tEnt) == TYPE, "paGetDefType: %d", TYPE);
}

void paGetDefReserve_test() {
    _Setup();
    p(paGetDefReserve(tEnt) == RESERVE, "paGetDefReserve: %d", RESERVE);
}

void paGetDefUpgrades_test() {
    _Setup();
    int result = paGetDefUpgrades(tEnt);  // MORE OF THIS
    p(result == UPBIT, "paGetDefUpgrades: %d == %d", result, UPBIT);
}

void paGetDefUpgradeClip_test() {
    _Setup();
    p(paGetDefUpgradeClip(tEnt) == UPCLIP, "paGetDefUpgradeClip: %d", UPCLIP);
}

void paGetDefaults_test() {
    _Setup();

    paGetDefaults(tEnt, _clip, _type, _reserve, _upbit, _upclip);
    p(
        _clip == CLIP
        && _type == TYPE
        && _reserve == RESERVE
        && _upbit == UPBIT
        && _upclip == UPCLIP,
        "paGetDefaults %d %d %d %d %d", _clip, _type, _reserve, _upbit, _upclip
    );
}

void paGetWeaponFeatures_test() {
    _Setup();

    paGetWeaponFeatures(tEnt, _clip, _type, _reserve, _upbit, _upclip);
    p(
        _clip == CLIP
        && _type == TYPE
        && _reserve == RESERVE
        && _upbit == UPBIT
        && _upclip == UPCLIP,
        "paGetWeaponFeatures %d %d %d %d %d", _clip, _type, _reserve, _upbit, _upclip
    );
}

void paSetWeaponFeatures_test() {
    _Setup(1, 1, 3, 4, 0);  // can't just change type?

    paGetWeaponFeatures(tEnt, _clip, _type, _reserve, _upbit, _upclip);
    p(
        _clip == 1
        && _type == 1
        && _reserve == 3
        && _upbit == 4
        && _upclip == 0,
        "paSetWeaponFeatures %d %d %d %d %d", _clip, _type, _reserve, _upbit, _upclip
    );
}

void paGetClip_test() {
    _Setup();
    p(paGetClip(tEnt) == CLIP, "paGetClip: %d", CLIP);
}

void paGetType_test() {
    _Setup();
    p(paGetType(tEnt) == TYPE, "paGetType: %d", TYPE);
}

void paGetReserve_test() {
    _Setup();
    p(paGetReserve(tEnt) == RESERVE, "paGetReserve: %d", RESERVE);
}

void paGetUpgrades_test() {
    _Setup();
    p(paGetUpgrades(tEnt) == UPBIT, "paGetUpgrades: %d", UPBIT);
}

void paGetUpgradeClip_test() {
    _Setup();
    p(paGetUpgradeClip(tEnt) == UPCLIP, "paGetUpgradeClip: %d", UPCLIP);
}

void paSetClip_test() {
    _Setup(2, 1, 3, 4, 2);
    paSetClip(tEnt, 2);
    p(paGetClip(tEnt) == 2, "paSetClip: %d", 2);
}

void paSetType_test() {
    _Setup(2, 1, 3, 4, 2);
    p(paGetType(tEnt) == 1, "paSetType: %d", 1);
}

void paSetReserve_test() {
    _Setup(2, 1, 3, 4, 2);
    p(paGetReserve(tEnt) == 3, "paSetReserve: %d", 3);
}

void paSetUpgrades_test() {
    _Setup(2, 1, 3, 4, 2);
    p(paGetUpgrades(tEnt) == 4, "paSetUpgrades: %d", 4);
}

void paSetUpgradeClip_test() {
    _Setup(2, 1, 3, 4, 2);
    p(paGetUpgradeClip(tEnt) == 2, "paSetUpgradeClip: %d", 2);
}

void paHasLaser_test() {
    _Setup();
    p(!paHasLaser(tEnt), "paHasLaser: false");
}

void paHasExplosive_test() {
    _Setup();
    p(!paHasExplosive(tEnt), "paHasExplosive: false");
}

void paHasIncendiary_test() {
    _Setup();
    p(!paHasIncendiary(tEnt), "paHasIncendiary: false");
}

void paSetLaser_test() {
    _Setup(); paSetLaser(tEnt);
    p(paHasLaser(tEnt), "paHasLaser: true");
}

void paSetExplosive_test() {
    _Setup(); paSetExplosive(tEnt);
    p(paHasExplosive(tEnt), "paHasExplosive: true");
}

void paSetIncendiary_test() {
    _Setup(); paSetIncendiary(tEnt);
    p(paHasIncendiary(tEnt), "paHasIncendiary: true");
}

void paDefrostWeapon_test() {
    _Setup();
    paFreezeWeapon(tEnt);
    p(paIsFrozen(tEnt), "paDefrostWeapon: Freeze Weapon");
    paDefrostWeapon(tEnt);
    p(paGetClip(tEnt) == 0 && paGetReserve(tEnt) == 0, "paDefrostWeapon: true");
}

void paFreezeWeapon_test() {
    _Setup();
    p(!paIsFrozen(tEnt), "paIsFrozen: false");
    paFreezeWeapon(tEnt);
    p( paIsFrozen(tEnt), "paIsFrozen: true");
}

void paCheckAndFreeze_test() {
    _Setup();
    p(!paCheckAndFreeze(tEnt), "paCheckAndFreeze: false");
    paFreezeWeapon(tEnt);
    p( paCheckAndFreeze(tEnt), "paCheckAndFreeze: true");
}

void paIsEntityUpgradeAmmo_test() {
    p(false, "NOT IMPLEMENTED");
}

void paCanUseUpgradeAmmo_test() {
    p(false, "NOT IMPLEMENTED");
}

void paIsWeapon_test() {
    _Setup();
    p(!paIsWeapon(tclient), "paIsWeapon: false");
    p(paIsWeapon(tEnt), "paIsWeapon: true");
}

void paIsEmpty_test() {
    _Setup();
    p(!paIsEmpty(tEnt), "paIsEmpty: false");
    _Setup(0, TYPE, 0);
    p( paIsEmpty(tEnt), "paIsEmpty: true");
}

void paIsFrozen_test() {
    _Setup();
    p(!paIsFrozen(tEnt), "paIsFrozen: false");
    paFreezeWeapon(tEnt);
    p( paIsFrozen(tEnt), "paIsFrozen: true");
}

void paPrintWeapon_test() {
    _Setup();
    paPrintWeapon(tEnt, "PrimeAres paPrintWeapon");
}

void paGetClient_test() {
    _Setup();
    int client = paGetClient(tEnt);
    p(client == tclient, "paGetClient");
}
